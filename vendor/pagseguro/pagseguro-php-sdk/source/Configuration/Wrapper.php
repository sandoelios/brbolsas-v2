<?php
/**
 * 2007-2016 [PagSeguro Internet Ltda.]
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author    PagSeguro Internet Ltda.
 * @copyright 2007-2016 PagSeguro Internet Ltda.
 * @license   http://www.apache.org/licenses/LICENSE-2.0
 *
 */
//toen oed
namespace PagSeguro\Configuration;

class Wrapper
{
    const ENV = "production";
    //const ENV = "sandbox";
    const EMAIL = "brbolsaspgseg@gmail.com";
    
    const TOKEN_PRODUCTION = "d934e908-450d-474c-83e7-30b08c673a96da5c7474493989ec614f4fb029302062a558-3226-48fe-822f-cfc11a3bce33";
    const APP_KEY_PRODUCTION = "C4CDD627E3E314D554817FBA198E5CFF";
    const APP_ID_PRODUCTION = "brbolsas";
    
    const TOKEN_SANDBOX = "58EB41AF55A64AC39F2F2F7E81AF3C04";
    const APP_ID_SANDBOX = "your_sandbox_application_id";
    const APP_KEY_SANDBOX = "your_sandbox_application_key";
    
    const CHARSET = "UTF-8";
    const LOG_ACTIVE = false;
    const LOG_LOCATION = "";
}
