<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<!DOCTYPE html>
<html >
    <head>
        <!--script para botao subir topo-->
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
         <script type="text/javascript">
               jQuery(document).ready(function(){

               

               jQuery('a#subirTopo').click(function () {
                 jQuery('body,html').animate({
                 scrollTop: 0}, 800);
                   return false;
               });

              jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > 1000) {
                jQuery('#subirTopo').fadeIn();
            } else {
               
            }
        });
    });
    </script>


        <!-- Site made with Mobirise Website Builder v4.9.1, https://mobirise.com -->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="generator" content="Mobirise v4.9.1, mobirise.com">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <meta name="description" content="">
        <title>BR Bolsas</title>
        <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
        <link rel="stylesheet" href="assets/tether/tether.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="assets/dropdown/css/style.css">
        <link rel="stylesheet" href="assets/socicon/css/styles.css">
        <link rel="stylesheet" href="assets/theme/css/style.css">
        <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/tether/tether.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/smoothscroll/smooth-scroll.js"></script>
        <script src="assets/dropdown/js/script.min.js"></script>
        <script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
        <script src="js/jquery.validate.js" type="text/javascript"></script>
        <script src="assets/parallax/jarallax.min.js"></script>
        <script src="assets/formoid/formoid.min.js"></script>
        <script src="https://rawgit.com/RobinHerbots/Inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
        <script src="js/app.js" type="text/javascript"></script>

    </head>
    <body>
        <section class="menu cid-rg0ia7LK31" once="menu" id="menu2-5">
            <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top collapsed">
                <!--<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                   <div class="hamburger red">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </button>-->
                <div class="menu-logo">
                    <div class="navbar-brand">
                        <span class="navbar-logo">
                            <a href="http://brbolsas.com.br">
                                <img src="assets/images/Marca.png" alt="BR BOLSAS" style="height: 3.8rem;">
                            </a>
                        </span>
                        <span class="navbar-caption-wrap">
                            <a class="navbar-caption text-black display-4" href="https://brbolsas.com.br">

                            </a>
                        </span>
                        <span class="align-left">
                            <a class="nav-link display-4" style=color:white align-left href="#" data-toggle="modal" data-target="#modal_home">
                                <h3>LOGIN</h3>
                            </a>
                         </span>
                        
                    </div>
                </div>
               <!-- <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true">
                       
                        <li class="nav-item">
                            <a class="nav-link link text-White display-4" href="#" data-toggle="modal" data-target="#modal_home">
                                ENTRAR
                            </a>
                        </li>                    

                    </ul>

                </div>-->
            </nav>
        </section>
        <div class="divider">&nbsp;
        </div>
    <br>
    
    
<div class="modal fade"   tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado"  id="modal_home" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="TituloModalCentralizado">Bem Vindo!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <p class="h4"> Olá, se você já tiver cadastro coloque seu CPF para logar</p>
          <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">CPF:</label>
            <input type="text" class="form-control" id="cpf_login_home" name="cpf_login_home" maxlength="14" >
          </div>
          
        </form>
          
      </div>
        <div class="">
            <p class="alert-danger" id="mensagem"></p>
        </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="btn_logar_home" disabled>Enviar</button>
                
      </div>
    </div>
  </div>
</div>

<div class="modal fade"   tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado"  id="modal_home_login" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="TituloModalCentralizado">Bem Vindo!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <p class="h4" id="texto_aluno_bolsa"> 
          </p>
        
          
      </div>
        <div class="">
            <p class="alert-danger" id="mensagem"></p>
        </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="home_logar">Enviar</button>
          
      </div>
    </div>
  </div>
</div>			