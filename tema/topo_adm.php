<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!DOCTYPE html>
<html >
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="generator" content="Mobirise v4.9.1, mobirise.com">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <meta name="description" content="">
        <title>BR Bolsas</title>

        <script src="../assets/web/assets/jquery/jquery.min.js"></script>

        <script src="js/jquery.validate.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../assets/web/assets/mobirise-icons/mobirise-icons.css">
        <link rel="stylesheet" href="../assets/tether/tether.min.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="../assets/dropdown/css/style.css">
        <link rel="stylesheet" href="../assets/socicon/css/styles.css">
        <link rel="stylesheet" href="../assets/theme/css/style.css">
        <link rel="stylesheet" href="../assets/mobirise/css/mbr-additional.css" type="text/css">
        <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="../assets/smoothscroll/smooth-scroll.js"></script>
        <script src="../assets/dropdown/js/script.min.js"></script>
        <link rel="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
        <script src="js/app.js" type="text/javascript"></script>
        <style>
            a{
                color: #ffffff;
                text-height: initial;
            }
            .menu-1{
                background-color: #002752
            }
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <section class="menu-1">
                <nav class="navbar navbar-expand-lg text-white">
                    <nav class="navbar ">
                        <a class="navbar-brand" href="#">
                            <img src="../assets/images/Marca.png"  class="d-inline-block align-top" style="height: 3.8rem;">

                        </a>
                    </nav>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse text-white" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="#"  style="color: #ffffff">CONFIGURAÇÕES<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    CADASTRO
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    
                                    <a class="dropdown-item" href="index.php?f=parceiros">Mantenedora</a>
                                    <a class="dropdown-item" href="index.php?f=cad_faculdade">Faculdades</a>
                                    <a class="dropdown-item" href="index.php?f=user">Usuários</a>
                                    <a class="dropdown-item" href="index.php?f=cad_bolsa">Bolsas</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    RELATÓRIOS
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="index.php?f=lista_alunos">Aluno</a>
                                    <a class="dropdown-item" href="index.php?f=lista_ies">Mantenedora</a>
                                    <a class="dropdown-item" href="index.php?f=lista_faculdade">Faculdades</a>
                                    <a class="dropdown-item" href="index.php?f=lista_bolsas">Bolsas</a>
                                    <a class="dropdown-item" href="index.php?f=lista_bolsas_aluno">Bolsas / Aluno</a>

                                </div>
                            </li>

                        </ul>

                    </div>
                </nav>
            </section>
             
        </div>