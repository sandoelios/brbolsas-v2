

$(document).ready(function () {

    $('#cep').inputmask('99.999-999');
    $("#cpf_login_home").inputmask('999.999.999-99');
    $('#cel').inputmask('(99)99999-9999');
    $('#cpf').inputmask('999.999.999-99');
    function login_home(){
        $('#modal_home').modal();
    }
    
     $("#cpf_login_home").on('change',function(){
        cpf = $("#cpf_login_home").val().replace(/[^\d]+/g,'')  ;
                
                
        if(validaCPF(cpf) === true){
            console.log('valido');
            $("#btn_logar_home").removeAttr("disabled");
            
        }
        else{
                $("#btn_logar_home").attr("disabled");
                alert('CPF Invalido');
            
        }
    });

    function validaCPF(cpf)
    {
        var numeros, digitos, soma, i, resultado, digitos_iguais;
        digitos_iguais = 1;
        if (cpf.length < 11)
            return false;
        for (i = 0; i < cpf.length - 1; i++)
            if (cpf.charAt(i) != cpf.charAt(i + 1))
            {
                digitos_iguais = 0;
                break;
            }
        if (!digitos_iguais)
        {
            numeros = cpf.substring(0, 9);
            digitos = cpf.substring(9);
            soma = 0;
            for (i = 10; i > 1; i--)
                soma += numeros.charAt(10 - i) * i;
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(0))
                return false;
            numeros = cpf.substring(0, 10);
            soma = 0;
            for (i = 11; i > 1; i--)
                soma += numeros.charAt(11 - i) * i;
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(1))
                return false;
            return true;
        } else
            return false;
    }
    

    $("#btn_logar_home").on("click", function () {
        var cpf = $("#cpf_login_home").val();

        if (cpf) {
            $.post("inc/ajax.php?func=validaCPF", {cpf: cpf})
                    .done(function (data)
                    {
                        r = JSON.parse(data);

                        if (r['status'] == 0)
                        {
                            alert('Continue seu cadastro.');
                            $('#modal').modal('hide');
                        } else
                        {
                            //PREENCHE A FICHA
                            $('#modal').modal('hide');
                            aluno = r['data'];

                            if (aluno['curso'] != "")
                            {
                                alert('Ol� ' + aluno['nome'] + ', Bem Vindo \n');
                                document.location.href = 'index.php?f=loga_aluno&data=' + aluno['id'];
                                //$("#form_alunos").hide();

                            }



                        }

                    });
        } else
        {
            alert('Digite um CPF Valido');
        }

    });

    function bolsasDinamica()
    {
        var cidade = $("#cidade_user").val();
        var url = "inc/ajax.php?func=bolsaRandon&data=" + cidade;
        if (cidade != "")
        {
            $.get(url, function (data)
            {
                var div_curso = $("#div_cont");
                div_curso.html("<h3 class='h3'>Pesquisando Bolsas na sua regi�o..</h3>");
                o = JSON.parse(data);
                if (o['status'] == 1)
                {
                    if (data['status'] == 0)
                    {
                        console.log('N�o Encontramos Unidades Nesta Regi�o');
                        return false;
                    }
                    obj = o['data'][0];
                    if (obj['id'])
                    {
                        console.log(obj['logo'])
                        id = obj['id'];
                        // $("#curso").append("<option value='" + obj['id'] + "'>" + obj['curso'] + "</option>");
                        bolsa = obj['bolsas'] % 7;
                        curso = obj['curso'];
                        var div = '<div class="row"  style="background: #ffc107; padding: 15px;">' +
                                "<div class='col-md-2'>" +
                                "<img class='card-img-top img-rounded' src='" + obj['logo'] + "' alt='Card image cap' style='width: 175px;height: 88px'></div>" +
                                "<div class='col-md-10'>" +
                                "<h6 class='mbr-bold mbr-fonts-style display-2'>" + obj['curso'] + "</h6><p class='mbr-fonts-style display-4'> " + obj['desc_curso'] + "</p>"
                                + "</div></div><div class='row' style='padding: 15px;background: #ffc107;'>" +
                                "<div class='col-md-2'>&nbsp;</div>" +
                                "<div class='col-md-1'><p class='h6'>  In�cio</p>" + obj['inicio'] +
                                "</div><div class='col-md-1'><p class='h6'>Turno</p>" + obj['turno'] + "</div><div class='col-md-1'><p class='h6'>   Dura��o</p>" + obj['duracao'] +
                                "</div>	<div class='col-md-1'><p class='h6'> Nivel</p>Bacharelado</div><div class='col-md-1'><p class='h6'> Modalidade <br> </p>Presencial" +
                                "</div><div class='col-md-1'><p class='h6'>Bolsa </p><b>" + obj['percentual'] + "</b></div>" +
                                "<div class='col-md-1'><p class='h6'> Mensalidade</p><del>" + obj['mensalidade'] +
                                "</del><br><b>" + obj['bolsa_desc'] + "</b></div>\n\
                                <div class='col-md-2'><button class='btn' value='" + obj['id'] + "' id='btn_mat' onclick='matricula(this.value)'>Entrar</button></div></div>";
                        div_curso.html(div);

                    }

                } else
                {
                    console.log(o);
                    return false;
                }
            });
        }


    }

    cidade = $("#cidade_user").val();
    if (cidade)
    {
        bolsasDinamica();

    }
    //********************** ** ** ** ** ** ** **
    $("#uf").on("change", function () {
        var estado = $("#uf").val();

        $.get("inc/ajax.php?func=unidades&data=" + estado, function (data) {
            if (data)
            {

                obj = JSON.parse(data);
                if (obj['status'] == 0)
                {
                    alert('N�o Encontramos Unidades Nesta Regi�o');
                    return false;
                }

                $("#cidade").empty();
                $("#cidade").append("<option value=''>Escolha Cidade</option>");
                for (i = 0; i < obj.length; i++)
                {
                    if (obj[i]['id'] !== undefined) {

                        $("#cidade").append("<option value='" + obj[i]['id'] + "'>" + obj[i]['nome'] + "</option>");
                    }
                }
            }
        });
    });

    $("#uf_n").on("change", function () {
        var estado = $("#uf_n").val();

        $.get("inc/ajax.php?func=unidades&data=" + estado, function (data) {
            if (data)
            {

                obj = JSON.parse(data);
                if (obj['status'] == 0)
                {
                    alert('N�o Encontramos Unidades Nesta Regi�o');
                    return false;
                }

                $("#cidade_n").empty();
                $("#cidade_n").append("<option value=''>Escolha Cidade</option>");
                for (i = 0; i < obj.length; i++)
                {
                    if (obj[i]['id'] !== undefined) {

                        $("#cidade_n").append("<option value='" + obj[i]['id'] + "'>" + obj[i]['nome'] + "</option>");
                    }
                }
            }
        });
    });

    $("#cidade").on('change', function () {
        var estado = $("#cidade").val();

        $.get("inc/ajax.php?func=faculdade&data=" + estado, function (data) {
            if (data)
            {
                obj = JSON.parse(data);

                if (obj['status'] == 0)
                {
                    alert('N�o Encontramos Unidades Nesta Regi�o');
                    return false;
                }
                if ($("#unidades option").length > 1)
                {
                    removeDivs($("#unidades option"));
                    $("#unidades").append("<option value=''>...</option>");
                }
                op = obj['data'];
                for (i = 0; i < op.length; i++)
                {
                    $("#unidades").append("<option value='" + op[i]['id'] + "'>" + op[i]['unidade'] + "</option>");
                }
            }
        });
    });

    $("#cidade_n").on('change', function () {
        var estado = $("#cidade_n").val();

        $.get("inc/ajax.php?func=faculdade&data=" + estado, function (data) {
            if (data)
            {
                obj = JSON.parse(data);

                if (obj['status'] == 0)
                {
                    alert('N�o Encontramos Unidades Nesta Regi�o');
                    return false;
                }
                if ($("#unidades_n option").length > 1)
                {
                    removeDivs($("#unidades_n option"));
                    $("#unidades_n").append("<option value=''>...</option>");
                }
                op = obj['data'];
                for (i = 0; i < op.length; i++)
                {
                    $("#unidades_n").append("<option value='" + op[i]['id'] + "'>" + op[i]['unidade'] + "</option>");
                }
            }
        });
    });

    $("#unidades").on('change', function () {
        var estado = $("#uf").val();
        var cidade = $("#cidade").val();
        var unidade = $("#unidades").val();
        var div_curso = $("#div-cursos");
        var url = "inc/ajax.php?func=bolsas&data=" + unidade;

        $.get(url, function (data)
        {

            o = JSON.parse(data);
            if (o['status'] == 1)
            {


                if (data['status'] == 0)
                {
                    alert('N�o Encontramos Unidades Nesta Regi�o');
                    return false;
                }
                if ($("#curso option").length > 1)
                {
                    removeDivs($("#curso option"));
                    $("#curso").append("<option value=''>...</option>");
                }
                obj = o['data'];
                for (i = 0; obj.length > i; i++)
                {
                    if (obj[i]['id'])
                    {
                        $("#curso").append("<option value='" + obj[i]['id'] + "'>" + obj[i]['curso'] + "</option>");

                    }
                }


            } else
            {
                alert('N�o Encontramos Unidades Nesta Regi�o');
                return false;
            }
        });

    });

    $("#unidades_n").on('change', function () {
        var estado = $("#uf_n").val();
        var cidade = $("#cidade_n").val();
        var unidade = $("#unidades_n").val();
        // var div_curso = $("#div-cursos_n");
        var url = "inc/ajax.php?func=bolsas&data=" + unidade;

        $.get(url, function (data)
        {

            o = JSON.parse(data);
            if (o['status'] == 1)
            {


                if (data['status'] == 0)
                {
                    alert('N�o Encontramos Unidades Nesta Regi�o');
                    return false;
                }
                if ($("#curso_novo option").length > 1)
                {
                    //removeDivs($("#curso option"));
                    $("#curso_novo").append("<option value=''>...</option>");
                }
                obj = o['data'];
                for (i = 0; obj.length > i; i++)
                {
                    if (obj[i]['id'])
                    {
                        $("#curso_novo").append("<option value='" + obj[i]['id'] + "'>" + obj[i]['curso'] + "</option>");

                    }
                }


            } else
            {
                alert('N�o Encontramos Unidades Nesta Regi�o');
                return false;
            }
        });

    });
    $("#curso").on('change', function ()
    {
        var curso = $("#curso").val();
 
        var div_curso = $("#div_cont");
        var url = "inc/ajax.php?func=bolsa&data=" + curso;
 
        $.get(url, function (data)
        {
         
            o = JSON.parse(data);
            if (o['status'] == 1)
            {
                if (data['status'] == 0)
                {
                    alert('Não Encontramos Unidades Nesta Região');
                    return false;
                }
                div_curso.html("<h3 class='h3'>Buscando Cursos..</h3>");
                obj = o['data'];
 
                if (obj['id'])
                {
                    id = obj['id'];
                    // $("#curso").append("<option value='" + obj['id'] + "'>" + obj['curso'] + "</option>");
                    bolsa = obj['bolsas'] % 7;
                    curso = obj['curso'];
 
                    var div = '<div class="row"  style="background: #ffc107; padding: 15px;">' +
                            //"<div class='col-md-1'>" +
                            //"<img class='card-img-top img-rounded' src='" + obj['logo'] + "' alt='' style='width: 175px;height: 88px'></div>" +
                            "<div class='col-md-12'>" +
                            "<h6 class='mbr-bold mbr-fonts-style display-2'>" + obj['curso'] + 
                            "</h6><p class='mbr-fonts-style display-4'> " + obj['desc_curso'] + "</p>"
 
                            + "</div></div><div class='row col-md-12' style='padding: 30px;background: #ffc107;'>" +
                            //"<div class='col-md-1'></div>" +
                            "<div class='col-md-2'><p class='h6' style='font-size:18px'><b>Bolsas Dispon�veis</b></p>" + obj['bolsa_disponivel'] +
                            "</div>"
                            +"<div class='col-md-2'><p class='h6' style='font-size:18px'><b>Turno</b></p>" + obj['turno'] 
                            + "</div>"
                            +"<div class='col-md-2'><p class='h6' style='font-size:18px'><b>Duração</b></p>" + obj['duracao'] + "  Semestre(s)" 
                            +"</div>"
                            +"<div class='col-md-2'><p class='h6' style='font-size:18px'><b> Graduação</b></p>Bacharelado</div>" 
                            +"<div class='col-md-2'><p class='h6' style='font-size:18px'> <b>Modalidade</b> <br> </p>Presencial" 
                            +"</div>"
                            +"<div class='col-md-2'><p class='h6' style='font-size:18px'><b>Percentual 1 e 2 Semestre</b> </p>" + obj['percentual'] + "%" + "</div>" 
                            +"<div class='col-md-2' style='margin-top:3%;'><p class='h6' style='font-size:18px'><b>Percentual apartir 3 Semestre</b> </p>" + obj['percentual_depois']+"%"+"</div>"
                             +"<div class='col-md-2' style='margin-top:3%;'><p class='h6' style='font-size:18px'> <b>Mensalidade</b></p><del>" +"R$ "+ obj['mensalidade'] + "</del><br>" + "R$ "+obj['bolsa_desc'] +
                             "</div>"
                             +"</div>"                         
                           +"<div class='col-md-2'><button class='btn' value='" + obj['id'] + "' id='btn_mat' onclick='matricula(this.value)'>Acessar</button></div></div>";

                            
                            
                           
                    div_curso.html(div);
                }
                // slickCarousel();
            } else
            {
                alert('N�o Encontramos Unidades Nesta Regi�o');
                return false;
            }
        });

    });
    /*  $("#logar").on("click", function () {
     var cpf = $("#cpf_login").val();
     if (cpf) {
     $.post("inc/ajax.php?func=validaCPF", {cpf: cpf})
     .done(function (data)
     {
     r = JSON.parse(data);
     
     if (r['status'] == 0)
     {
     alert('Continue seu cadastro.');
     $('#modal').modal('hide');
     } else
     {
     //PREENCHE A FICHA
     $('#modal').modal('hide');
     aluno = r['data'];
     
     if (aluno['curso'] != "")
     {
     alert('Ol� ' + aluno['nome'] + ', Bem Vindo \n');
     document.location.href = 'index.php?f=loga_aluno&data=' + aluno['id'];
     //$("#form_alunos").hide();
     
     }
     
     
     
     }
     
     });
     }
     
     });
     
     */
    $("#buscar").on("click", function ()
    {
        var estado = $("#uf").val();
        var cidade = $("#cidade").val();
        var unidade = $("#unidades").val();
        var url = "inc/ajax.php?func=bolsas&data=" + estado + "|" + cidade + '|' + unidade;
        var section = '<div class="customer-logos cid-rg0hEO5Sci" id="div-cursos"></div>';
        var div_curso = $("#div-cursos");

        $.get(url, function (data) {
            o = JSON.parse(data);
            //console.log(o['status']);return false;
            if (o['status'] == 1)
            {


                if (data['status'] == 0)
                {
                    alert('N�o Encontramos Unidades Nesta Regi�o');
                    return false;
                }

                if ($('.customer-logos').hasClass('slick-initialized') == true)
                {
                    destroyCarousel();


                }

                obj = o['data'];
                for (i = 0; obj.length > i; i++)
                {

                    if (obj[i]['id'])
                    {

                        bolsa = obj[i]['bolsas'] % 7;
                        curso = obj[i]['curso'];
                        div = '<div class="card col-auto" style="height: 280px;"><img class="card-img-top img-rounded" src="http://www.unirb.edu.br/img/logo.png" alt="Card image cap" style="width: 60%">' +
                                '<div class="card-body"><p class="card-text">' + '<h6>' + curso + '</h6><b>Valor: ' + obj[i]['mensalidade'] + '<br/>' + 'Bolsas: ' + bolsa + '<br/>' + 'Percentual: ' + obj[i]['percentual'] + '</b><br/>' +
                                '<small>' + obj[i]['unidade'] + '</small></p>' +
                                '</div></div></div>';
                        div_curso.append(div);


                    }
                }
                slickCarousel();

            } else
            {
                alert('N�o Encontramos Unidades Nesta Regi�o');
                return false;
            }
        });
    });


    function slickCarousel() {
        $('.customer-logos').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1500,
            arrows: false,
            dots: false,
            pauseOnHover: false,
            responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2
                    }
                }, {
                    breakpoint: 520,
                    settings: {
                        slidesToShow: 1
                    }
                }]
        });
    }
    function destroyCarousel() {
        if ($('.customer-logos').hasClass('slick-initialized')) {
            $('.customer-logos').slick('destroy');
            $('.customer-logos').slick('refresh');
        }
    }
    function getHandon(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    function removeDivs(div)
    {

        div.each(function (i, e) {
            e.remove();
        });
    }




});
function matricula(idCurso)
{
    url = "http://brbolsas.com.br/index.php?f=pre-matricula&data=" + idCurso;
    window.location.href = url;

}
function login()
{
    $("#modal_login").modal();

}
