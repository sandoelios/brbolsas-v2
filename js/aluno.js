$('#cep').inputmask('99.999-999');
$("#cpf_login").inputmask('999.999.999-99');
$('#telefone').inputmask('(99)9999-9999');
$('#cel').inputmask('(99)99999-9999');
$('#cpf').inputmask('999.999.999-99');


$("#salvar_aluno").on('click', function ()
{
    $("#form_alunos").validate({
        submitHandler: function (form) {
            form.submit();
        }
    });

});


$("#cpf").on("change", function () {
    var cpf = $("#cpf").val();
    if (cpf) {
        $.post("inc/ajax.php?f=validaCPF", {cpf: cpf})
                .done(function (data) {
                    console.log("Data Loaded: " + data);
                });
    }

});

function printData(div)
{
    
   newWin= window.open("");
   newWin.document.write(div.outerHTML);
   newWin.print();
   newWin.close();
}



$("#btn_imprimir").click(function () {
    
    var doc = $("#printable");
    console.log(doc);
    
   printData(doc[0]);
    
});
function imprimirVauche()
{
    cpf = $("#cpf").val();
    window.location.href = "sys/aluno/vauche.php?cpf="+cpf+"&f=imprimir-vauche";
    
    
}

$("#update_aluno").on("click", function () {

    nome = $("#nome").val();
    cpf = $("#cpf").val();
    email = $("#email").val();
    cel = $("#cel").val();
    tel = $("#telefone").val();
    id = $("#id_aluno").val();
   

    $.post("inc/ajax.php?func=update-aluno", {
        cpf: cpf,
        nome: nome,
        cel: cel,
        telefone: tel,
        id_aluno: id,
        email: email
    }).done(function (data)
    {
        r = JSON.parse(data);
        console.log(r);
        if(r.status == 1)
        {
                alert('Dados Alualizado com sucesso!');
        }else{
                alert('Não foi possível atualair registro.')
        }
        

    });

});
function financeiro()
{

    $("#financeiro").attr('hidden', false);
    $("#curso_inscrito").attr('hidden', true);
    $("#dados_pessoais").attr('hidden', true);
    $("#novo_curso").attr('hidden', true);

}
function minhaInscricao()
{
    $("#financeiro").attr('hidden', true);
    $("#curso_inscrito").attr('hidden', false);
    $("#dados_pessoais").attr('hidden', true);

}
function meusDados()
{
    $("#novo_curso").attr('hidden', true);
    $("#curso_inscrito").attr('hidden', true);
    $("#financeiro").attr('hidden', true);
    $("#dados_pessoais").attr('hidden', false);

}
function novoCuros()
{
    $("#curso_inscrito").attr('hidden', true);
    $("#financeiro").attr('hidden', true);
    $("#dados_pessoais").attr('hidden', true);
    $("#novo_curso").attr('hidden', false);

}

function bolsanova()
{
    id_bolsa = $("#curso_novo").val();
    cpf = $("#cpf").val();
    if (cpf != "" || id_bolsa != "")
    {
        $.post("inc/ajax.php?func=bolsaNova", {
            cpf: cpf,
            bolsa: id_bolsa
        })
                .done(function (data) {
                    //console.log(data);
                    if (data['status'] == 1) {
                        alert('Bolsa Atualizada');
                        document.location.reload(true);
                    } else {
                        alert('Erro ao atualizar bolsa');
                    }
                });
    } else
    {
        alert('Escolha um curso');
    }
}

function gerarPagamento() {
    var meioP = $("#meioPagamento").val();
    window.location.href = "http://brbolsas.com.br/checkout/index.php";
}

$("#curso_novo").on('change', function ()
{
    var curso = $("#curso_novo").val();

    var div_curso = $("#div_cont_n");
    var url = "inc/ajax.php?func=bolsa&data=" + curso;

    $.get(url, function (data)
    {


        o = JSON.parse(data);
        if (o['status'] == 1)
        {
            if (data['status'] == 0)
            {
                alert('Não Encontramos Unidades Nesta Região');
                return false;
            }
            div_curso.html("<h3 class='h3'>Buscando Cursos..</h3>");
            obj = o['data'];

            if (obj['id'])
            {
                id = obj['id'];
                // $("#curso").append("<option value='" + obj['id'] + "'>" + obj['curso'] + "</option>");
                bolsa = obj['bolsas'] % 7;
                curso = obj['curso'];

                var div = '<div class="row"  style="background: #ffc107; padding: 15px;">' +
                        "<div class='col-md-2'>" +
                        "<img class='card-img-top img-rounded' src='" + obj['logo'] + "' alt='Card image cap' style='width: 175px;height: 88px'></div>" +
                        "<div class='col-md-10'>" +
                        "<h6 class='mbr-bold mbr-fonts-style display-2'>" + obj['curso'] + "</h6><p class='mbr-fonts-style display-4'> " + obj['desc_curso'] + "</p>"

                        + "</div></div><div class='row' style='padding: 15px;background: #ffc107;'>" +
                        "<div class='col-md-2'>&nbsp;</div>" +
                        "<div class='col-md-1'><p class='h6'>  Início</p>" + obj['inicio'] +
                        "</div><div class='col-md-1'><p class='h6'>Turno</p>" + obj['turno'] + "</div><div class='col-md-1'><p class='h6'>   Duração</p>" + obj['duracao'] + " Anos" +
                        "</div>	<div class='col-md-1'><p class='h6'> Nivel</p>Bacharelado</div><div class='col-md-1'><p class='h6'> Modalidade <br> </p>Presencial" +
                        "</div><div class='col-md-1'><p class='h6'>Bolsa </p><b>" + obj['percentual'] + "</b></div>" +
                        "<div class='col-md-2'><p class='h6'> Mensalidade</p><del>" + obj['mensalidade'] + "</del><br><b>" + obj['bolsa_desc'] +
                        "</b></div><div class='col-md-2'><button class='btn' value='" + obj['id']
                        + "' id='btn_mat' onclick='bolsanova(this.value)'>Alterar</button></div></div>";
                div_curso.html(div);
            }
            // slickCarousel();
        } else
        {
            alert('Não Encontramos Unidades Nesta Região');
            return false;
        }
    });

});

$("#logar").on("click", function () {
    var cpf = $("#cpf_login").val();
    console.log(cpf);
    if (cpf) {
        $.post("inc/ajax.php?func=validaCPF", {cpf: cpf})
                .done(function (data)
                {
                    r = JSON.parse(data);

                    if (r['status'] == 0)
                    {
                        alert('Continue seu cadastro.');
                        $('#modal').modal('hide');
                    } else
                    {
                        //PREENCHE A FICHA
                        $('#modal').modal('hide');
                        aluno = r['data'];

                        if (aluno['curso'] != "")
                        {
                            alert('Olá ' + aluno['nome'] + ', Bem Vindo \n');
                            document.location.href = 'index.php?f=loga_aluno&data=' + aluno['id'];
                            //$("#form_alunos").hide();

                        }



                    }

                });
    }
    else
    {
        alert('Digite um CPF Valido');
    }

});

$('#modal').modal();

