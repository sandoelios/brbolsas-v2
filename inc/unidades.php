<?php
$faculdades = array(
	array(
		"id" => 1,
		"name" => "UNIRB | Centro Universit�rio Regional do Brasil",
	),
	array(
		"id" => 2,
		"name" => "UNIRB | EAD Unirb",
	),
	array(
		"id" => 3,
		"name" => "UNIRB | Faculdade UNIRB - Feira de Santana",
	),
	array(
		"id" => 4,
		"name" => "UNIRB | FBT - Faculdade Brasileira de Tecnologia",
	),
	array(
		"id" => 5,
		"name" => "UNIRB | Faculdade Regional de Alagoinhas",
	),
	array(
		"id" => 6,
		"name" => "UNIRB | Faculdade UNIRB - Barreiras",
	),
	array(
		"id" => 7,
		"name" => "UNIRB | Faculdade UNIRB Aracaju",
	),
	array(
		"id" => 8,
		"name" => "UNIRB | Faculdade Brasileira de Tecnologia",
	),
	array(
		"id" => 9,
		"name" => "UNIRB | FARB - Faculdade Regional Brasileira",
	),
	array(
		"id" => 10,
		"name" => "UNIRB | Faculdade UNIRB - Arapiraca",
	),
	array(
		"id" => 11,
		"name" => "UNIRB | Faculdade UNIRB - Parnaíba",
	),
	array(
		"id" => 12,
		"name" => "UNIRB | Faculdade UNIRB - Natal",
	),
	array(
		"id" => 13,
		"name" => "UNIRB | Faculdade UNIRB - Mossoró",
	),
	array(
		"id" => 14,
		"name" => "UNIRB | Faculdade UNIRB - Piauí",
	),
	array(
		"id" => 15,
		"name" => "UNIRB | Faculdade UNIRB - Fortaleza",
	),
	array(
		"id" => 16,
		"name" => "UNIRB | Faculdade UNIRB - Serrinha",
	),
	array(
		"id" => 17,
		"name" => "UNIRB | Faculdade UNIRB - Teresina ",
	),
);
?>