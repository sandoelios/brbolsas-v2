<?php

$estados = getEstados($con);
//var_dump($_REQUEST);
   if(!empty($_GET['id']))
   {
       $query = "select * from ies ie where id=".$_GET['id'];   
       $rs  = $con->query($query);
       $ies = $rs->fetch_object();
       
       $cidade = getCidades($con,$ies->id_estado);
   } 
?>

<div class="divider">&nbsp;
</div>
<form class="form-horizontal" style="margin: 100px 100px 100px 100px;"  id="parceiros_update" action="../sys/ies/cad/update_ies.php" method="POST" enctype="multipart/form-data">
    <fieldset>

        <!-- Form Name -->
        <div class="form-group">
            <div class="col-md-8">
                <p class="form-group control-label h4 h4">Atualização de Parceiros</p><br>
            </div>
        </div>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="textinput">Nome da Instituição *</label>  
            <div class="col-md-8">
                <input id="nome" name="nome" type="text" placeholder="Digite seu nome" class="form-control input-md required" value="<?=$ies->nome?>">
                <input id="id_ies" name="id_ies" type="text" placeholder="Digite seu nome" class="form-control input-md required" value="<?=$ies->id?>" hidden="">

            </div> 
        </div>
         <div class="form-group">
            <label class="col-md-4 control-label h4" for="textinput">Logo da Instituição *</label>  
            <div class="col-md-8">
                <input id="logo" name="logo" type="file" placeholder="Escolha uma imagem" class="form-control input-md" >

            </div>
        </div>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label h4 h4" for="textinput">CNPJ</label>  
            <div class="col-md-8">
                <input id="cnpj" name="cnpj" value="<?=$ies->cnpj?>" type="text" placeholder="00.000.000/0000-00" class="form-control input-md required" maxlength="13">

            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label h4" for="emal">Email *</label>  
            <div class="col-md-8">
                <input id="email" name="email" value="<?=$ies->email?>" type="text" placeholder="Digite um email valido" class="form-control input-md required email">

            </div>
        </div>
                
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="origem_aluno">Modalidade *</label>
            <div class="col-md-8">
                <select id="modalidade" name="modalidade[]" class="form-control required" multiple="">
                    <?php
                    $md = explode(',', $ies->modalidade);
                    foreach ($md as $m)
                    {
                        $ck[$m] = "selected"; 
                    }
                    
                    ?>
                    <option value="1" <?=$ck[1] ?>>GRADUAÇÃO</option>
                    <option value="2" <?=$ck[2] ?>>PÓS GRADUAÇÃO</option>
                    <option value="3" <?=$ck[3] ?>>CURSOS TÉCNICO</option>
                    <option value="4" <?=$ck[4] ?>>IDIOMAS</option>
                </select>
            </div>
        </div>
      <div class="form-group">
            <label class="col-md-4 control-label h4" for="id_estado">* Estado</label>  
            <div class="col-md-8">
                
                <select id="id_estado" name="id_estado" class="form-control required">
                    <option value="">...</option>
                    <?php foreach ($estados as $e):?>
                    <option value="<?=$e['id']?>" <?=($e['id']== $ies->id_estado)?'selected':''  ?>><?= $e['nome']?></option>
                    <?php endforeach;?>
                </select>
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="id_municipio">* Cidade</label>  
            <div class="col-md-8">
                
                <select id="id_municipio" name="id_municipio" class="form-control required">
                    <option value="0">...</option>
                    <?php
                        foreach ($cidade as $c):
                    ?>
                    <option value="<?=$c['id']?>" <?= ($c['id'] == $ies->id_municipio)? 'selected' : '' ?>> 
                                <?=$c['nome']?> 
                    </option>
                    
                    <?php
                        endforeach;
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="end">Endereço *</label>  
            <div class="col-md-8">
                <input id="email" name="end" value="<?=$ies->end?>" type="end" placeholder="Digite endereço com rua complemento e número" class="form-control input-md required">

            </div>
        </div>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="telefone">* Telefone</label>  
            <div class="col-md-8">
                <input id="telefone" name="telefone" value="<?=$ies->telefone?>" type="text"  class="form-control input-md required">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="responsavel">* Responsável</label>  
            <div class="col-md-8">
                <input id="responsavel" value="<?=$ies->responsavel?>" name="responsavel" type="text" placeholder="" class="form-control input-md required">

            </div>
        </div>

        <!-- Textarea -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="textarea">Mensagem</label>
            <div class="col-md-8">                     
                <textarea class="form-control" id="textarea" name="textarea" style="height: 300px;">
 
                </textarea>
                
            </div>
        </div>
       
        <!-- Button -->
        <div class="form-group">

            <div class="col-md-4">
                <button id="atualiza_ies" name="salvar_ies" class="btn btn-primary">Enviar</button>
            </div>
        </div>

    </fieldset>
</form>
<script src="https://rawgit.com/RobinHerbots/Inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.9.0/jquery.validate.min.js"></script>
<script>
$("#telefone").inputmask("(99)9999-9999");
$("#cnpj").inputmask("99.999.999.999/99");

</script>
<script src="js/ies.js" type="text/javascript"></script>
