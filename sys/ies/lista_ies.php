 

<div class="container-fluid">
  <h2>MANTENEDORAS</h2>
  
  <table class="table" id="tabela">
    <thead>
      <tr>
        <th>ID</th>
        <th>IES</th>
        <th>ENDEREÇO</th>
        <th>CNPJ</th>
        <th>RESPONSÁVEL</th>
        <th>EMAIL</th>
      </tr>
    </thead>
    <tbody>
       <?php 
       $query = "select id,nome,cnpj,end,email,responsavel from ies order by nome asc";
       $rs = $con->query($query);
       
       while ($ies = $rs->fetch_assoc()):
       ?>
        <tr>
            <td>
                <a style="color: #000"href="index.php?f=update_ies&id=<?=$ies['id']?>">  
                <?=$ies['id']?> 
                </a>
            </td>
        <td><?=$ies['nome']?></td>
        <td><?=$ies['end']?></td>
        <td><?=$ies['cnpj']?></td>
        <td><?=$ies['responsavel']?></td>
        <td><?=$ies['email']?></td>
        </tr>
        <?php endwhile;?>
    </tbody>
  </table>
</div>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>

$('#tabela').dataTable( {
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
            }
        } );
</script>
</script>