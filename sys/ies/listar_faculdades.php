 

<div class="container-fluid">
  <h2>FACULDADES</h2>
  
  <table class="table" id="tabela">
    <thead>
      <tr>
        <th>ID</th>
        <th>FACULDADE</th>
        <th>ENDEREÇO</th>
        <th>EMAIL</th>
        <th>RESPONSÁVEL</th>
        <th>MANTENEDORA</th>
      </tr>
    </thead>
    <tbody>
       <?php 
require '../../inc/bd.php';
       $query = "SELECT u.id, u.faculdade,u.endereco,u.email,m.nome cidade,e.uf,i.nome mantenedora,u.responsavel
                FROM unidades u 
                INNER JOIN ies i ON i.id=u.id_ies 
                inner join municipios m on m.codigo_ibge=u.id_municipio
                inner join estados e on e.codigo_uf=u.id_estado";
       $rs = $con->query($query);
       
       while ($ies = $rs->fetch_assoc()):
       ?>
        <tr>
        <td><?=$ies['id']?></td>
        <td><?=$ies['faculdade']?></td>
        <td><?= $ies['endereco'] . " ".$ies['cidade']." " .$ies['uf']?></td>
        <td><?=$ies['email']?></td>
        <td><?=$ies['responsavel']?></td>
        <td><?=$ies['mantenedora']?></td>
        </tr>
        <?php endwhile;?>
        
        <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        </tr>
    </tbody>
  </table>
</div>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>


<script>

$('#tabela').dataTable( {
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
            },
                "scrollY":        "380px",
                "scrollCollapse": true,
                "paging":         true,
                "scrollX": true
                
        } );
</script>
</script>