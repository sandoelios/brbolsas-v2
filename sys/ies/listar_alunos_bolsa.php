<?php
require '../../inc/bd.php';
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
 <style>
        .paginate_button{
            color:red;
            pading:1;
        }
 </style>
<div class="container-fluid">
 <table   class="table table-striped table-bordered" id="tabela">
    <thead>
      <tr>
        <th>ALUNO</th>
        <th>CPF</th>
        <th>CELULAR</th>
        <th>EMAIL</th>
        <th>CIDADE</th>
        <th>ESTADO</th>
        <th>FACULDADE</th>
        <th>BOLSAS</th>
        <th>STATUS</th>     
        <th>TIPO PAGAMENTO</th>
        
        
      </tr>
    </thead>
    <tbody>
       <?php 
       $query ="SELECT p.nome aluno,p.cpf_cnpj,p.cel,p.email,p.cidade,e.nome,uni.faculdade,ba.id_status_transacao,be.curso,ba.meio_de_pagamento
       FROM pessoa p
       JOIN bolsa_aluno  ba on p.cpf_cnpj = ba.cpf_aluno
       JOIN estados e ON p.uf = e.codigo_uf
       JOIN bolsas_estudo be ON ba.id_bolsa = be.id
       JOIN unidades uni ON be.id_unidade = uni.id";
     
       $rs = $con->query($query);
       
       while ($ies = $rs->fetch_assoc()):
           
       ?>
        <tr>
        
        <td><?=$ies['aluno']?></td>
        <td><?=$ies['cpf_cnpj']?></td>
        <td><?=$ies['cel']?></td>
        <td><?=$ies['email']?></td>
        <td><?=$ies['cidade']?></td>
        <td><?=$ies['nome']?></td>     
        <td><?=$ies['faculdade']?></td>
        <td><?=$ies['curso']?></td>
        <td><?=$ies['id_status_transacao']?></td>
        <td><?=$ies['meio_de_pagamento']?></td>
        
        </tr>
        <?php endwhile;?>
    </tbody>
  </table>
</div>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>


<script>
$('#tabela').dataTable( {
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
            },
                "scrollY":        "380px",
                "scrollCollapse": true,
                "paging":         true,
                "scrollX": true
                
        } );
</script>
</script>