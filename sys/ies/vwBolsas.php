
<div class="divider">&nbsp;
</div>
<form class="form-horizontal" style="margin: 100px 100px 100px 100px;" id="cadFaculdade" id="formCurso" action="../sys/ies/cad/cad_curso.php" method="POST">
    <fieldset>
        <div class="form-group">
            <div class="col-md-8">
                <p class="form-group control-label h4 h4">Cadastro de Bolsas</p><br>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="faculdade">Faculdade</label>  
            <div class="col-md-8">
                
                <select id="faculdade" name="faculdade" class="form-control" required email>
                    <option value="">...</option>
                    <?php foreach ($unidades as $e):?>
                    <option value="<?=$e['id']?>"><?= $e['faculdade']?></option>
                    <?php endforeach;?>
                </select>
            </div>
        </div>
      
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="curso">Curso </label>  
            <div class="col-md-8">
                <input id="cad_curso" name="cad_curso" type="text" class="form-control input-md" required>

            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="duracao">Duração</label>  
            <div class="col-md-8">
                <input id="duracao" name="duracao" type="text" class="form-control input-md" required email>

            </div>
        </div>
                
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="modalidade">Modalidade </label>
            <div class="col-md-8">
                <select id="modalidade" name="modalidade[]" class="form-control" required multiple>
                    <option value="1">EAD</option>
                    <option value="2">Presencial</option>
                    <option value="3">Semipresencial</option>
                </select>
            </div>
        </div>
      
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="turno"> Turno</label>  
            <div class="col-md-8">
                
                <select id="turno" name="turno[]" class="form-control" required multiple="" required>                    
                    <option value="1">Matutino</option>
                    <option value="2">Vespertino</option>
                    <option value="3">Noturno</option>
                </select>
                
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="parcela">Parcelas </label>  
            <div class="col-md-8">
                <input id="parcela" name="parcela" type="text" class="form-control input-md" required>
            </div>
        </div>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="mensalidade" > Mensalidade</label>  
            <div class="col-md-8">
                <input id="mensalidade" name="mensalidade" type="text"  class="form-control input-md" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="desconto" > Desconto</label>  
            <div class="col-md-8">
                <input id="desconto" name="desconto" type="text"  class="form-control input-md" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="vagas"> Vagas</label>  
            <div class="col-md-8">
                <input id="vagas" name="vagas" type="number"   class="form-control input-md" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="inicio" > Inicio</label>  
            <div class="col-md-8">
                <input id="inicio" name="inicio" type="text"  class="form-control input-md" required placeholder="Ano/Semestre">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="desc_curso" > Apresentção do Curso</label>  
            <div class="col-md-8">
                <textarea id="desc_curso" name="desc_curso" style="width: 100%;height:200px;" class="form-control input-md" required=""></textarea>
            </div>
        </div>
        
        <!-- Button -->
        <div class="form-group">
            <div class="col-md-4">
                <button id="salvar_curso" name="salvar_curso" class="btn btn-primary">Enviar</button>
            </div>
        </div>

    </fieldset>
</form>
<script src="js/ies.js" type="text/javascript"></script>