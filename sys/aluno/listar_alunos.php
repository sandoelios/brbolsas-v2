<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="container-fluid">
  <h2>ALUNOS</h2>
  
  <table class="table" id="tabela">
    <thead>
      <tr>
        <th>ID</th>
        <th>ALUNO</th>
        <th>EMAIL</th>
        <th>CPF</th>
        <th>CIDADE</th>
        <th>ESTADO</th>
        <th>BOLSA</th>
      </tr>
    </thead>
    <tbody>
       <?php 
       $query = "select p.id,p.nome aluno ,cpf_cnpj cpf, email, e.nome uf,cidade,curso
from pessoa p
inner join estados e on p.uf=e.codigo_uf
left join bolsa_aluno ba on ba.cpf_aluno=p.cpf_cnpj
left join bolsas_estudo be on be.id=ba.id_bolsa
 order by p.nome asc LIMIT 1000";
       $rs = $con->query($query);
       
       while ($ies = $rs->fetch_assoc()):
       ?>
        <tr>
        <td><?=$ies['id']?></td>
        <td><?=$ies['aluno']?></td>
        <td><?=$ies['email']?></td>
        <td><?=$ies['cpf']?></td>
        <td><?=$ies['cidade']?></td>
        <td><?=$ies['uf']?></td>
        <td><?=$ies['curso']?></td>
        </tr>
        <?php endwhile;?>
    </tbody>
  </table>
</div>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>


<script>

$('#tabela').dataTable( {
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
            }
        } );
</script>
</script>