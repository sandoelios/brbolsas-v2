   <?php
   
   //var_dump($estados);exit;
   if($_GET['data'])
   {
        $string = "select id,curso from bolsas_estudo where id=".$_GET['data'];
        $rs     = $con->query($string);
        $bolsa  = $rs->fetch_all(MYSQLI_ASSOC);
   
   }
   else {
        echo "<script>alert('Selecione Um curso antes');<script>";
        header("Location: ./index.php");
    }
       
?>
<div class="modal fade"   tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado"  id="modal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="TituloModalCentralizado">Bem Vindo!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <p class="h4"> Olá, se você já tiver cadastro coloque seu CPF para logar</p>
          <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">CPF:</label>
            <input type="text" class="form-control" id="cpf_login" name="cpf_login" maxlength="14" >
          </div>
          
        </form>
          
      </div>
        <div class="">
            <p class="alert-danger" id="mensagem"></p>
        </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="logar">Enviar</button>
                
      </div>
    </div>
  </div>
</div>

<div class="modal fade"   tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado"  id="modal_aluno_cadastrado" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="TituloModalCentralizado">Bem Vindo!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <p class="h4" id="texto_aluno_bolsa"> 
          </p>
        
          
      </div>
        <div class="">
            <p class="alert-danger" id="mensagem"></p>
        </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="logar">Enviar</button>
          
      </div>
    </div>
  </div>
</div>

<div class="divider">&nbsp;
</div>
<form class="form-horizontal" style="margin: 100px 100px 100px 100px;" action="sys/aluno/inc/cad_aluno.php" method="POST" id="form_alunos" name="form_alunos">
    <fieldset>

        <!-- Form Name -->
        <div class="form-group">
            <div class="col-md-8">
                <p class="form-group control-label h4 h4">Cadastro de Alunos</p><br>
            </div>
        </div>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="nome">Nome*</label>  
            <div class="col-md-8">
                <input id="nome" name="nome" type="text" placeholder="Digite seu nome" class="form-control input-md required">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label h4 h4" for="textinput">* CPF</label>  
            <div class="col-md-8">
                <input id="cpf" name="cpf" type="text" placeholder="000.000.000-00" class="form-control input-md required">

            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label h4" for="email">* Email</label>  
            <div class="col-md-8">
                <input id="email" name="email" type="text" placeholder="Digite um email valido" class="form-control input-md required">

            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="curso">* Curso</label>
            <div class="col-md-8">
                <select   id="curso" name="curso" class="form-control" required="">
                    <option value="<?php echo $bolsa[0]['id']?>" selected="">
                            <?php echo $bolsa[0]['curso']?>
                    </option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="cel">* Residencial</label>  
            <div class="col-md-8">
                <input id="telefone" name="telefone" type="text" placeholder="(99)9999-9999" class="form-control input-md">
                <span class="help-block">*Mantenha seus contatos atualizados</span>  
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="cel">* Celular</label>  
            <div class="col-md-8">
                <input id="cel" name="cel" type="text" placeholder="(99)99999-9999" class="form-control input-md">
                <span class="help-block">*Mantenha seus contatos atualizados</span>  
            </div>
        </div>
         <div class="form-group">
            <label class="col-md-4 control-label h4" for="data_nasc">* Data de Nascimento</label>  
            <div class="col-md-8">
                <input id="data_nasc" name="data_nasc" type="date" placeholder="" class="form-control input-md required">

            </div>
        </div>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="uf">* Estado</label>  
            <div class="col-md-8">
                <select id="uf" name="uf" class="form-control required">
               <?php
                foreach ($estados['data'] as $es):
               ?>
                    <option value="<?=$es['id']?>"> <?= $es['nome']?></option>
                
                <?php endforeach;?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="cidade">* Cidade</label>  
            <div class="col-md-8">
                <input id="aluno_cidade" name="aluno_cidade" type="text" placeholder="Salvador" class="form-control input-md required">

            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="rua">* Rua</label>  
            <div class="col-md-8">
                <input id="rua" name="rua" type="text" placeholder="" class="form-control input-md required">

            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="cep">* CEP</label>  
            <div class="col-md-8">
                <input id="cep" name="cep" type="text" placeholder="" class="form-control input-md required">

            </div>
        </div>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="bairro">* Bairro</label>  
            <div class="col-md-8">
                <input id="bairro" name="bairro" type="text" placeholder="" class="form-control input-md required">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="numero">* Número</label>  
            <div class="col-md-8">
                <input id="numero" name="numero" type="text" placeholder="" class="form-control input-md required">

            </div>
        </div>

        <!-- Button -->
        <div class="form-group col-md-12">
            <div class="col-md-4">
                <button  style= "background-color:#ffc107;width:65%" class = "btn"><a href="http://brbolsas.com.br/files/MINUTA%20DO%20CONTRATO.pdf" target="_blank" style="color:white">Minuta</a></button>
            </div>
            <div class="col-md-6">
                <button id="salvar_aluno" name="salvar" style ="width:42%" class="btn btn-primary">Enviar</button>
            </div>       
        </div>

    </fieldset>
</form>

  <script src="https://rawgit.com/RobinHerbots/Inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
<script src="js/aluno.js" type="text/javascript"></script>
