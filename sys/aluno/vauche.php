<?php

require_once './inc/bd.php';
require_once './inc/funcoes.php';
$id = $_SESSION['id'];
$query = "
SELECT p.id, p.nome, cpf_cnpj as cpf, p.email, cel,cidade,e.nome estado,rua,bairro,curso ,faculdade, id_transacao,
case 
when	be.modalidade = '1' then 'EAD' 
when	be.modalidade = '2' then 'Presemcial' 
when	be.modalidade = '3' then 'Semipresencial' 
when	be.modalidade = '1,2' then 'EAD e Presencial' 
when	be.modalidade = '1,3' 
then 'EAD e Semipresencial' end, 
case when turno = '1' then 'Matutino' 
when turno = '2' then 'Verpertino' 
when turno = '3' then 'Noturno' 
when turno = '1,2' then 'Matutino e Vespertino' 
when turno = '1,3' then 'Matutino e Noturno' 
end turno,
es.desc_status,ba.id as id_bolsa ,mensalidade,percentual,logo_url,duracao, 
format( mensalidade - (percentual / 100) * mensalidade,2) bolsa_desc 
FROM pessoa p 
inner join estados e on e.codigo_uf=p.uf 
left join bolsa_aluno ba on ba.cpf_aluno=p.cpf_cnpj 
left join bolsas_estudo be on be.id=ba.id_bolsa 
left join unidades u on u.id=be.id_unidade 
left join transacoes tr on tr.cpf_aluno=p.cpf_cnpj
left join tb_status es on es.id = ba.status 
left join ies ie on ie.id=u.id_ies  and ba.status not in(3,4)
WHERE p.id=$id order by ba.data_cad asc
limit 1";
;
try {
    $rs = $con->query($query);
    $aluno = $rs->fetch_object();
} catch (Exception $ex) {
    print_r($query);
    exit;
}
 
?>
<div class="row" id="curso_inscrito">

    <div class="col-md-12 col-sm-12">
        <div class="card border-success text-black mb-6"  style="width: 100%;">
            <div class="card-header">
                <h3>Curso: <?= $aluno->curso ?></h3>
            </div>
            <div class="card-body border-success">
                <h5 class="card-title">Faculdade : <?= $aluno->faculdade ?></h5>
                <h5 class="card-title">Turno : <?= $aluno->turno ?></h5>
                <h5 class="card-title">Mensalidade : <?= $aluno->mensalidade ?></h5>
                <h5 class="card-title">Com desconto : <?= $aluno->bolsa_desc ?></h5>
                <h5 class="card-title">Bolsa : <?= $aluno->percentual ?></h5>
                <h5 class="card-title">Situação da Bolsa : <?= $aluno->desc_status ?></h5>
               
            </div>
            
            <div class="card-footer border-success align-right">
                <button type="button" class="btn btn-danger text-black" id="escolher_curso" onclick="window.print()" >Imprimir Vauche</button>
            </div>
        </div>
    </div>
    <div class="divider">  
        &nbsp;
    </div>

</div>
<br/>