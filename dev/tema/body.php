 

<section class="cid-rg0jDSoXm9 mbr-fullscreen" id="header15-8">
    <div class="align-left ">
        <div class="col-12 align-left bolsas" data-form-type="formoid">
            <div class="mbr-white col-lg-8 col-md-12 align-left" >
                <h1 class=" mbr-bold mbr-fonts-style display-5">
                    <br>
                    Bolsas de até 50%
                </h1>
                <p class="mbr-text pb-3 ">
                    Use o formulário abaixo para pesquisar bolsas disponíveis em sua cidade.
                </p>
            </div>
            <form class="mbr-form" action="/" method="post" data-form-title="Mobirise Form">
                <div data-for="UF" class="form-group">
                    <select id="uf" name="uf" class="form-control" >
                        <option value="" selected="">Escolha um Estado</option>
                        <?php foreach ($estados['data'] as $e): ?>
                            <option value=<?= $e['id'] ?>" >
                            <?= $e['nome'] ?>
                                    </option>
                                <?php endforeach; ?>
                                </select>
                                </div>
                                <div class="form-group">
                    <select id="cidade" name="cidade" class="form-control">
                        <option>Escolha uma Cidade</option>
                    </select>
            </div>  
            <div class="form-group">
                <select id="unidades" name="unidades" class="form-control">
                    <option>Escolha um Faculdade</option>
                </select>
            </div>
            <div class="form-group">
                <select id="curso" name="curso" class="form-control">
                    <option>Escolha um Curso</option>
                </select>
            </div>
            <br>
            <div class="form-group">
                &nbsp;
            </div>
        </form>
    </div>
</div>
</div>

</section>
<div class="container-fluid">
    <h2>Cursos</h2>
</div>

<div class="container-fluid  cid-rg0hEO5Sci" id="div_cont">
    <div class="row"  style="background: #ffc107; padding: 15px;">
        <p><strong>Portaria do MEC: </strong>275, 20/07/2011&nbsp; &nbsp; &nbsp; &nbsp;<strong>Vagas:</strong>&nbsp;160 vagas anuais</p> <p class="rtejustify"><strong>Turno:</strong> Matutino e Noturno &nbsp; &nbsp; &nbsp;&nbsp;<strong>Dura&ccedil;&atilde;o:</strong>&nbsp;4 anos</p> <p class="rtejustify"><strong>Dirigentes da Institui&ccedil;&atilde;o</strong>:&nbsp;Carlos Joel Pereira, Diretor Geral;</p> <p class="rtejustify" style="padding-left: 30px;">Ailda Pereira, Diretora Administrativo Financeira;</p> <p class="rtejustify" style="padding-left: 30px;">Ana Costa, Superintendente da Unidade.</p> <p class="rtejustify"><strong>Mensalidade:</strong> R$ 1.368,00</p> <p class="rtejustify">&nbsp;</p> <p align="justify">O Curso de Gradua&ccedil;&atilde;o de Nutri&ccedil;&atilde;o da UNIRB foi concebido em conson&acirc;ncia com as Diretrizes Curriculares Nacionais (parecer CNE/CES 1.1133/2001) apresentando uma vis&atilde;o general&iacute;stica, human&iacute;stica e cr&iacute;tica, objetivando a forma&ccedil;&atilde;o de profissionais &eacute;ticos, respons&aacute;veis socialmente e com&nbsp;<strong>habilidades e compet&ecirc;ncias</strong>para a atua&ccedil;&atilde;o nas diversas&nbsp;<strong>&aacute;reas da nutri&ccedil;&atilde;o</strong></p> <p align="justify">O&nbsp;<strong>Curr&iacute;culo do Curso de Nutri&ccedil;&atilde;o da UNIRB</strong>&nbsp;contempla &agrave; identidade dos cursos de Nutri&ccedil;&atilde;o de &acirc;mbito nacional, &agrave;s crescentes e din&acirc;micas transforma&ccedil;&otilde;es no campo da Nutri&ccedil;&atilde;o e &agrave;s especificidades das demandas locais. Resumidamente o curr&iacute;culo &eacute; composto de disciplinas do n&uacute;cleo b&aacute;sico de forma&ccedil;&atilde;o; disciplinas profissionalizantes; est&aacute;gios curriculares supervisionados; trabalho de conclus&atilde;o do curso (TCC) e atividades complementares.</p> <p align="justify">Quanto &agrave;s demandas nacionais e internacionais, as pesquisas na &aacute;rea da sa&uacute;de p&uacute;blica mostram um perfil epidemiol&oacute;gico nutricional e da situa&ccedil;&atilde;o alimentar preocupante, suscitando a participa&ccedil;&atilde;o dos nutricionistas em: pol&iacute;ticas, programas e a&ccedil;&otilde;es voltadas para o coletivo; no combate das defici&ecirc;ncias nutricionais; e no combate aos agravos associados diretamente a transi&ccedil;&atilde;o demogr&aacute;fica e ao envelhecimento populacional, tais como: excesso de peso, diabetes, hipertens&atilde;o, dentre outros; no desenvolvimento de novos estudos.</p> <p align="justify">Em rela&ccedil;&atilde;o &agrave;s demandas locais, apontamos o fato do estado da Bahia, e especialmente a cidade de Salvador destacar-se nacionalmente no turismo. Essa posi&ccedil;&atilde;o no turismo amplia ainda mais o mercado de trabalho do nutricionista, seja no setor comercial - hot&eacute;is, restaurantes &ndash; seja na vigil&acirc;ncia sanit&aacute;ria. Acrescentamos ainda que segundo dados do IBGE (2002), 32% da popula&ccedil;&atilde;o apresenta uma m&eacute;dia salarial inferior a um sal&aacute;rio m&iacute;nimo, sendo esta parcela populacional uma clientela de atendimento nos servi&ccedil;os p&uacute;blicos, particularmente nos servi&ccedil;os de sa&uacute;de.</p> <p align="justify">O Curso de Nutri&ccedil;&atilde;o da UNIRB apresenta uma proposta inovadora e pertinente, disponibilizando no 1o. semestre, disciplinas fundamentais para a forma&ccedil;&atilde;o acad&ecirc;mica e profissional: Hist&oacute;ria da Nutri&ccedil;&atilde;o e Metodologia do Trabalho Cient&iacute;fico. A primeira oferece bases para uma an&aacute;lise hist&oacute;rica da nutri&ccedil;&atilde;o enquanto ci&ecirc;ncia e profiss&atilde;o e ainda possibilita ao aluno &agrave; compreens&atilde;o do projeto pedag&oacute;gico do curso e da sua futura profiss&atilde;o. Estrategicamente a Disciplina Metodologia do Trabalho Cient&iacute;fico, encontra-se alocada no in&iacute;cio do curso visando fornecer conhecimentos que permitam aos graduandos desenvolver habilidades l&oacute;gica, metodol&oacute;gica e cr&iacute;tica, estimulando &ldquo;precocemente&rdquo; condutas pautadas em evid&ecirc;ncias cient&iacute;ficas.<br /><br />A UNIRB implantar&aacute; o<strong>&nbsp;NIPESNUT</strong>&nbsp;(N&uacute;cleo Integrado de Pesquisa, Ensino e Servi&ccedil;os em Nutri&ccedil;&atilde;o), agregando as atividades de pesquisa, ensino e extens&atilde;o, TCC, atividades complementares e inicia&ccedil;&atilde;o cient&iacute;fica, e tamb&eacute;m atendendo aos compromissos sociais, no est&iacute;mulo &agrave; pesquisa e na presta&ccedil;&atilde;o de servi&ccedil;o &agrave; sociedade, atuando em todas as fases de promo&ccedil;&atilde;o a sa&uacute;de.</p> <p>&nbsp;&nbsp;</p> <p>&nbsp;</p>
    </div>

    <div class="container-fluid">
        <div class="row cid-rg0hEO5Sci">
            <div class="col-12">

            </div>
        </div>
    </div>

    <section class="features3 cid-rg0hB15I8t" id="features3-2">




        <div class="container">
            <div class="media-container-row"  >


            </div>
        </div>
    </section>