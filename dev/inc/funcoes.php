<?php

function auth($con, $req)
{
    $query = "select desc_perm from permissao p";
}
function Limpar($string, $padrao, $replace) {

return preg_replace($padrao, $replace, $string);

}
function escape_str($string)
{
//var_dump($string);exit;
require '../../../inc/bd.php';
return mysqli_real_escape_string($con, $string);

}
function disciplinasRandon($con, $cidade) {
if ($con) {
if ($cidade != null) {
$query = "select be.id,curso,case
                        when turno ='1'
                        then 'Matutino'
                        when turno ='2' 
                        then 'Vespertino'
                        when turno ='3'
                        then 'Noturno'
                        when turno ='1,2' 
                        then 'Matutino<br>Vespertino'
                        when turno ='1,3' 
                        then 'Matutino<br>Noturno'
                        when turno ='1,2,3'
                        then 'Matutino<br>Vespertino<br>Noturno'
                        end as turno,
                        duracao,parcelas,mensalidade,percentual,faculdade unidade,vagas,desc_curso,DATE_FORMAT(inicio,'%m/%Y') inicio,format( mensalidade - (percentual / 100) * mensalidade,2) bolsa_desc,logo_url 
                from bolsas_estudo be 
                inner join unidades u on u.id=be.id_unidade 
                inner join estados e on e.codigo_uf =u.id_estado
                left join ies ie on u.id_ies=ie.id
                where  e.nome='$cidade' ";
// print_r($query);exit;
} else {
$msg = array("msg" => 'NÃO FOI ENCONTRADO DADOS PARA OS PARÂMETROS PASSADO', "status" => 0);
return $msg;
}

$rs = $con->query($query);
// print_r($query);exit;
if ($rs->num_rows > 0) {
$estados['data'] = $rs->fetch_all(MYSQLI_ASSOC);
$estados['status'] = 1;
return $estados;
} else {
$msg = array("msg" => 'NÃO FOI ENCONTRADO DADOS PARA OS PARÂMETROS PASSADO', "status" => 0);
return $msg;
}
}
}

function getEstados($con) {

$query = "select codigo_uf as id,nome from estados WHERE codigo_uf in(27,29,23,22,24) order by nome asc";
$rs = $con->query($query);

$estados['data'] = $rs->fetch_all(MYSQLI_ASSOC);
$estados['status'] = 1;
return $estados;
}
function updateAluno($con, $dados)
{
    $cpf = Limpar($dados['cpf'], '/[^0-9$]/', '');
    unset($dados['cpf']);
    $fields =" ";
    $f = count($dados);
    foreach ($dados as $i => $d)
    {
        if(!is_null($d) and $f >1 and $i != 'cpf')
        {
            
            $fields .=" $i ='$d',";  
            
        }
        else {
           $fields .=" $i ='$d'";
        }
        
        --$f;
    }
    $query = "update pessoa set ".$fields. " where cpf_cnpj='".$cpf."'";
    //print_r($query);exit;
    if($rs = $con->query($query))
    {
        $ret['msg'] = "Registro atualizado com sucesso";
        $ret['status'] = 1;
        return $ret;
    }
    else {
        $ret['msg'] = "Erro ocorrido ".$query;
        $ret['status'] = 0;
        return $ret; 
    }


}


function bolsaNova($con, $data) {

if($data)
{


$query = "update  bolsa_aluno set status=3 where cpf_aluno='".$data['cpf']."'";
if($rs = $con->query($query))
{
$query = "INSERT INTO bolsa_aluno (id_bolsa,cpf_aluno,status,data_expira) values(".$data['bolsa'].",'".$data['cpf']."',1,date_add(now(), interval 4 day))";
try {
$rs = $con->query($query);
} catch (Exception $ex) {
$ret['msg'] = $ex;
$ret['status'] = 0;
return $ret;
}
$ret['msg'] = "Nova Bolsa Cadastrada";
$ret['status'] = 1;
return $ret;
}
else
{
$ret['msg'] = "Houve um erro ao atualizar bolsa".$query;
$ret['status'] = 0;
return $ret;
}


}
else {
$ret['msg'] = "Dados Obrigatório não foram fornecido, favor escolher uma bolsa que esteja disponivel.";
$ret['status'] = 0;
return $ret;
}

}
function getIES($con) {
if (!$con)
return "Falta A conexão!";
$query = "select * from ies order by nome asc";
$rs = $con->query($query);
$estados = $rs->fetch_all(MYSQLI_ASSOC);
$estados['status'] = 1;
return $estados;
}

function getCidades($con, $id) {

$query = "select codigo_ibge id,nome from municipios m where m.codigo_uf=$id
                 order by nome asc ";
$rs = $con->query($query);
$estados = $rs->fetch_all(MYSQLI_ASSOC);
$estados['status'] = 1;
return $estados;
}

function getUnidades($con, $id) {
if ($id and $con) {
$query = "select codigo_ibge as id,nome from municipios c where c.codigo_uf=" . $id;

$rs = $con->query($query);


$ret['status'] = 1;
if ($rs->num_rows > 0) {
$ret['data'] = $rs->fetch_all(MYSQLI_ASSOC);

$ret['status'] = 1;
return $ret;
} else {
$msg = array("msg" => 'NÃO FOI ENCONTRADO DADOS PARA OS PARÂMETROS PASSADO', "status" => 0);
return $msg;
}
}
}

function getBolsas($con, $unidade) {
if ($con) {
if ($unidade != null) {
$query = "select be.id,curso,case when turno ='1' then 'Matutino' when turno ='2' then 'Vespertino' when turno ='3' then 'Noturno' when turno ='1,2' then 'Matutino,Vespertino' when turno ='1,3' then 'Matutino,Noturno' when turno ='1,2,3' then 'Matutino,Vespertino,Noturno' 
end as turno,duracao,parcelas,mensalidade,percentual,faculdade unidade,bolsa, DATE_FORMAT(inicio,'%m/%Y') inicio,
format( mensalidade - (percentual / 100) * mensalidade,2) bolsa_desc, logo_url,case when be.modalidade ='1' then 'EAD' when be.modalidade ='2' then 'Presencial' when be.modalidade ='3' then 'Semi-presencial' end as nivel
from bolsas_estudo be 
inner join unidades u on u.id=be.id_unidade 
left join ies i on i.id=be.id_unidade where  be.id_unidade=$unidade and bolsa_disponivel >0";
//   print_r($query);
} else {

}

$rs = $con->query($query);
//  print_r($query);exit;
if ($rs->num_rows > 0) {
$estados['data'] = $rs->fetch_all(MYSQLI_ASSOC);
$estados['status'] = 1;
return $estados;
} else {
$msg = array("msg" => 'NÃO FOI ENCONTRADO DADOS PARA OS PARÂMETROS PASSADO', "status" => 0);
return $msg;
}
}
}

function getBolsa($con, $curso) {
if ($con) {
$query = "select be.id,curso,case
                        when turno ='1'
                        then 'Matutino'
                        when turno ='2' 
                        then 'Vespertino'
                        when turno ='3'
                        then 'Noturno'
                        when turno ='1,2' 
                        then 'Matutino,Vespertino'
                        when turno ='1,3' 
                        then 'Matutino,Noturno'
                        when turno ='1,2,3'
                        then 'Matutino,Vespertino,Noturno'
                        end as turno,duracao,parcelas,DATE_FORMAT(inicio,'%m/%Y') inicio ,mensalidade,percentual,
                        faculdade unidade,bolsa,desc_curso,format( mensalidade - (percentual / 100) * mensalidade,2) bolsa_desc,logo_url,case when be.modalidade ='1' then 'EAD' when be.modalidade ='2' then 'Presencial' when be.modalidade ='3' then 'Semi-presencial' end as nivel
              from bolsas_estudo be inner join unidades u on u.id=be.id_unidade  left join ies ie on u.id_ies=ie.id 
              where  be.id=$curso and be.bolsa_disponivel>0";

$rs = $con->query($query);
//  print_r($query);exit;
if ($rs->num_rows > 0) {
$estados['data'] = $rs->fetch_all(MYSQLI_ASSOC);
$estados['status'] = 1;
return $estados;
} else {
$msg = array("msg" => 'NÃO FOI ENCONTRADO DADOS PARA OS PARÂMETROS PASSADO', "status" => 0);
return $msg;
}
}
}

function getFaculdade($con, $cidade) {
if ($cidade and $con) {
$query = "select distinct id,faculdade from unidades  u
            where  id_municipio=" . $cidade;

$rs = $con->query($query);

if ($rs->num_rows > 0) {
$estados['data'] = $rs->fetch_all(MYSQLI_ASSOC);
$estados['status'] = 1;
return $estados;
} else {
$msg = array("msg" => 'NÃO FOI ENCONTRADO DADOS PARA OS PARÂMETROS PASSADO', "status" => 0);
return $msg;
}
} else {
$query = "select distinct id,faculdade from unidades ";

$rs = $con->query($query);

if ($rs->num_rows > 0) {
$estados = $rs->fetch_all(MYSQLI_ASSOC);
//$estados['status'] = 1;
return $estados;
} else {
$msg = array("msg" => 'NÃO FOI ENCONTRADO DADOS PARA OS PARÂMETROS PASSADO', "status" => 0);
return $msg;
}
}
}

 