<div class="divider">&nbsp;
</div>
<form class="form-horizontal" style="margin: 100px 100px 100px 100px;">
    <fieldset>

        <!-- Form Name -->
        <div class="form-group">
            <div class="col-md-8">
                <p class="form-group control-label h4 h4">Cadastro de Bolsas</p><br>
            </div>
        </div>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="textinput">Curso *</label>  
            <div class="col-md-8">
                <input id="textinput" name="textinput" type="text" placeholder="Digite seu nome" class="form-control input-md">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label h4 h4" for="textinput">Turno</label>  
            <div class="col-md-8">
                 <select id="origem_aluno" name="origem_aluno" class="form-control">
                    <option value="">...</option>
                    <option value="1">MATUTINO</option>
                    <option value="2">VESPERTINO</option>
                    <option value="3">NOTURNO</option>
                    <option value="4">M/V/N</option>
                </select>

            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label h4" for="emal">* Mensalidade</label>  
            <div class="col-md-8">
                <input id="mensalidade" name="mensalidade" type="number" placeholder="Valor total da Bolsa" class="form-control input-md">

            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="origem_aluno">Unidade *</label>
            <div class="col-md-8">
                <select id="unidade" name="unidade" class="form-control">
                    <option value="0">...</option>
                    <option value="1">GRADUAÇÃO</option>
                    <option value="2">PÓS GRADUAÇÃO</option>
                    <option value="3">CURSOS TÉCNICO</option>
                    <option value="">IDIOMAS</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="cel">* Duração</label>  
            <div class="col-md-8">
                <input id="cel" name="cel" type="text" placeholder="4 anos" class="form-control input-md">
                <span class="help-block">*Mantenha seus contatos atualizados</span>  
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="uf">* Semestre</label>  
            <div class="col-md-8">
                <input id="uf" name="uf" type="text" placeholder="2019.2" class="form-control input-md">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="cidade">* Percentual</label>  
            <div class="col-md-8">
                <input id="cidade" name="cidade" type="text" placeholder="30%" class="form-control input-md">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="bairro">* Quantidade de Vagas</label>  
            <div class="col-md-8">
                <input id="bairro" name="bairro" type="text" placeholder="" class="form-control input-md">

            </div>
        </div>

        <!-- Textarea -->
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="bairro">* Descrição do Curso</label>  
            <div class="col-md-8">                     
                <textarea class="form-control" id="textarea" name="textarea" style="height: 300px;">
 
                </textarea>
                
            </div>
        </div>
       
        <!-- Button -->
        <div class="form-group">

            <div class="col-md-4">
                <button id="salvar" name="salvar" class="btn btn-primary">Enviar</button>
            </div>
        </div>
        
    </fieldset>
</form>
<script src="js/app.js" type="text/javascript"></script>