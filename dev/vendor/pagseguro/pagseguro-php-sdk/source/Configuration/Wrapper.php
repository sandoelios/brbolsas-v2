<?php
/**
 * 2007-2016 [PagSeguro Internet Ltda.]
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author    PagSeguro Internet Ltda.
 * @copyright 2007-2016 PagSeguro Internet Ltda.
 * @license   http://www.apache.org/licenses/LICENSE-2.0
 *
 */
//toen oed
namespace PagSeguro\Configuration;

class Wrapper
{
//    const ENV = "production";
    const ENV = "sandbox";
    const EMAIL = "adalgisio01@yahoo.com.br";
    
    const TOKEN_PRODUCTION = "49ad1991-2a4f-4ccd-844c-beaa8c43f5ee5558b77b4ebd92cac90330327bc7b03fb45b-30f8-4ba5-a020-2a5114b58af2";
    const APP_KEY_PRODUCTION = "2D240C500F0FF98BB441DF900FDFD5D8";
    const APP_ID_PRODUCTION = "brbolsa_hml";
    
    const TOKEN_SANDBOX = "58EB41AF55A64AC39F2F2F7E81AF3C04";
    const APP_ID_SANDBOX = "your_sandbox_application_id";
    const APP_KEY_SANDBOX = "your_sandbox_application_key";
    
    const CHARSET = "UTF-8";
    const LOG_ACTIVE = false;
    const LOG_LOCATION = "";
}
