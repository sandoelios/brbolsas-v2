<?php
session_start();
$id = $_GET['data'];
$query = "
SELECT p.id, p.nome, cpf_cnpj as cpf, p.email, cel,cidade,e.nome estado,rua,bairro,curso ,faculdade, 
case 
when	be.modalidade = '1' then 'EAD' 
when	be.modalidade = '2' then 'Presemcial' 
when	be.modalidade = '3' then 'Semipresencial' 
when	be.modalidade = '1,2' then 'EAD e Presencial' 
when	be.modalidade = '1,3' 
then 'EAD e Semipresencial' end, 
case when turno = '1' then 'Matutino' 
when turno = '2' then 'Verpertino' 
when turno = '3' then 'Noturno' 
when turno = '1,2' then 'Matutino e Vespertino' 
when turno = '1,3' then 'Matutino e Noturno' 
end turno,
es.desc_status,ba.id as id_bolsa ,mensalidade,percentual,logo_url,duracao, 
format( mensalidade - (percentual / 100) * mensalidade,2) bolsa_desc 
FROM pessoa p 
left join estados e on e.codigo_uf=p.uf 
left join bolsa_aluno ba on ba.cpf_aluno=p.cpf_cnpj 
left join bolsas_estudo be on be.id=ba.id_bolsa 
left join unidades u on u.id=be.id_unidade 
left join transacoes tr on tr.cpf_aluno=p.cpf_cnpj
left join tb_status es on es.id = ba.status 
left join ies ie on ie.id=u.id_ies  and ba.status not in(3,4)
WHERE p.id=$id order by ba.data_cad asc
limit 1";


try {
    $rs = $con->query($query);
    $aluno = $rs->fetch_object();
} catch (Exception $ex) {
    print_r($ex);
    exit;
}
$_SESSION['aluno'] = $aluno->nome;
$_SESSION['id'] = $aluno->id;
$_SESSION['cpf'] = $aluno->cpf;
$_SESSION['id_bolsa'] = $aluno->id_bolsa;

$query = "select  date_format(data_criacao,'%d/%m/%Y %H:%i')  data_transacao,id_transacao,data_retorno data_pagamento,desc_status,curso,
            tmp.desc_tipo_meio, ba.data_cad as data_cad_bolsa,date_format(data_expira,'%d/%m/%Y %H:%i') data_expira,
            u.endereco, u.faculdade,u.telefone, e.uf, m.nome municipio,u.email
            from 
            transacoes t
            left join tipo_meio_pagamento tmp on tmp.cod_meio = t.cod_meio_pagamento
            left join tb_status ts on ts.id=t.status
            left join bolsa_aluno ba on ba.cpf_aluno=t.cpf_aluno
            left join bolsas_estudo be on be.id=ba.id_bolsa and t.status=ba.status
            left join pessoa p on t.cpf_aluno=p.cpf_cnpj
            left join unidades u on u.id=be.id_unidade
            left join estados e on e.codigo_uf=u.id_estado
            left join municipios m on m.codigo_ibge=u.id_municipio
            where p.id=$aluno->id and ba.status in (1,2,6)";
$rs = $con->query($query);

$df[] = $rs->fetch_array(MYSQLI_ASSOC);
//var_dump($df);exit;
?> 
<br>
<nav class="nav nav-pills flex-column flex-sm-row justify-content-end" style="padding: 20px;">
    <a class="flex-sm-fill text-sm-center nav-link " href="#" id="btn_dados" onclick="meusDados()" >Meus Dados</a>
    <a class="flex-sm-fill text-sm-center nav-link"  href="#" id="btn_insc" onclick="minhaInscricao()">Minha Inscrição</a>
    <a class="flex-sm-fill text-sm-center nav-link"  href="#" id="btn_fin" onclick="financeiro()">Financeiro</a>
    <a class="flex-sm-fill text-sm-center nav-link"  href="#" id="btn_segunda_via">2ª Via de Documentos</a>
    <a class="navbar-brand" href="#">
        <i class="mbri-smile-face"></i>
        <?= $aluno->nome ?>

    </a>
</nav>

<div class="row" id="curso_inscrito">

    <div class="col-md-12 col-sm-12">
        <div class="card border-success text-black mb-6"  style="width: 100%;">
            <div class="card-header">
                <h3>Curso: <?= $aluno->curso ?></h3>
            </div>
            <div class="card-body border-success">
                <h5 class="card-title">Faculdade : <?= $aluno->faculdade ?></h5>
                <h5 class="card-title">Turno : <?= $aluno->turno ?></h5>
                <h5 class="card-title">Mensalidade : <?= $aluno->mensalidade ?></h5>
                <h5 class="card-title">Com desconto : <?= $aluno->bolsa_desc ?></h5>
                <h5 class="card-title">Bolsa : <?= $aluno->percentual ?></h5>
                <h5 class="card-title">Situação da Bolsa : <?= $aluno->desc_status ?></h5>
                <h5 class="card-title">
                    <div class="radio">
                        <label> Boleto : &nbsp;<input type="radio" name="meioPagamento" id="meioPagamento" value="boleto"> </label> 
                         <label> Cartão : &nbsp;<input type="radio" name="meioPagamento" id="meioPagamento" value="cartao"> </label>
                    </div>
                </h5>

            </div>

            <div class="card-footer border-success align-right">
                <?php
                if ($aluno->desc_status == "PAGO") {
                    ?>                
                    <button type="button" class="btn btn-danger text-black" id="escolher_curso"  data-toggle="modal" data-target="#exampleModalLong" >Imprimir Vauche</button>
                <?php } else {
                    ?>
                    <button type="button" class="btn btn-danger text-black" onclick="gerarPagamento()">Pagar</button>
                    <button type="button" class="btn btn-danger text-black" onclick="novoCuros()" id="escolher_curso">Mudar Curso</button>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="divider">  
        &nbsp;
    </div>

</div>
<?php if ($aluno->desc_status == "PAGO") { ?>
    <br/>

    <div id="vauche">
        <div class="modal fade bd-example-modal-lg" id="exampleModalLong"   tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document" >
                <div class="modal-content">
                    <div id="printable">
                        <div class="modal-header">
                            <img src="assets/images/Marca.png" class="img" style="width: 20%">

                        </div>
                        <div class="modal-body text-left">
                            <h3>Comprovante de Pagamento</h3>
                            <br/>
                            Parabéns,<b> <?= $aluno->nome ?>,</b> dirija-se a unidade de ensino, 
                            <?= $df[0]["faculdade"] . ", " . $df[0]["endereco"] . ", " . $df[0]["municipio"] . ", " . $df[0]["uf"] ?>, 
                            portando os documentos necessários para conlusão de sua matricula<br/>
                            <br/>
                            <h3>Contatos</h3> 
                            <br/>
                            Telefone: <?= $df[0]["telefone"] ?><br/>
                            Email: <?= $df[0]["email"] ?> <br/>
                            Curso: <?= $df[0]["faculdade"] ?><br/>
                            Chave de transação: <?= $df[0]["id_transacao"] ?><br/>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="btn_imprimir">Imprimir</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php } ?>

<div class="divider">
    &nbsp;
</div>
<div class="row" id="financeiro" hidden="">

    <table class="table" id="tb_financeiro">
        <thead class="thead-light">
            <tr>      
                <th scope="col">Data do Cadastro</th>
                <th scope="col">Curso</th>
                <th scope="col">Data Vencimento</th>
                <th scope="col">Valor</th>
                <th scope="col">Forma Pagamento</th>
                <th scope="col">Situação</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($df as $d):
                // print_r($d);
                ?>
                <tr>
                    <td><?= $d["data_transacao"] ?></td>
                    <td><?= $d["curso"] ?></td>
                    <td><?= $d["data_expira"] ?></td>
                    <td><?= $d["desc_status"] ?></td>
                    <td><?= $d["desc_tipo_meio"] ?></td>
                    <td><?= $d["desc_status"] ?></td>
                </tr>
                <?PHP
            endforeach;
            ?>

        </tbody>
    </table>

</div>

<br/>
<div id="dados_pessoais" hidden="" style="background-color: lightgreen">
    <form class="bg-info" id="form_dpessoais">
        <div class="form-group">
            <label for="nome" class="col-sm-2 col-form-label">Nome</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="nome" value="<?= $aluno->nome ?>">
                <input type="hidden" class="form-control" id="id_aluno" value="<?= $aluno->id ?>">
            </div>
        </div>
        <div class="form-group ">
            <label for="cpf" class="col-sm-2 col-form-label">CPF</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="cpf" value="<?= $aluno->cpf ?>" disabled="">
            </div>
        </div>
        <div class="form-group ">
            <label for="email" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" id="email" value="<?= $aluno->email ?>">
            </div>
        </div>
        <div class="form-group ">
            <label for="telefone" class="col-sm-2 col-form-label">Telefone</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="telefone" value="<?= $aluno->telefone ?>">
            </div>
        </div>
        <div class="form-group ">
            <label for="cel" class="col-sm-2 col-form-label">Celular</label>
            <div class="col-sm-10">
                <input type="tel" class="form-control" id="cel" value="<?= $aluno->cel ?>">
            </div>
        </div>


        <div class="form-group row">
            <div class="col-sm-10">
                <input type="button" id="update_aluno"class="btn btn-primary" value="Salvar">
            </div>
        </div>
    </form>
</div>

<div class="row" id="novo_curso" hidden="" >

    <section class="" id="" style="background-color: #ffffff">
        <div class="align-left ">
            <input type="text" id="cidade_user" name="cidade_user" value="<?= $cidade ?>" hidden="">
            <div class="col-12 align-left bolsas" data-form-type="formoid">
                <div class="mbr-white col-lg-8 col-md-12 align-left" >
                    <h1 class=" mbr-bold mbr-fonts-style display-5">
                        <br>
                        Bolsas de até 50%
                    </h1>
                    <p class="mbr-text pb-3 ">
                        Use o formulário abaixo para pesquisar bolsas disponíveis em sua cidade.
                    </p>
                </div>
                <form class="mbr-form" action="/" method="post">
                    <div data-for="UF" class="form-group">
                        <select id="uf_n" name="uf_n" class="form-control">
                            <option value="">Escolha um Estado</option>
                            <?php foreach ($estados['data'] as $e): ?>
                                <option value="<?php echo $e['id'] ?>"><?php echo $e['nome'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select id="cidade_n" name="cidade_n" class="form-control">
                            <option>Escolha uma Cidade</option>
                        </select>
                    </div>  
                    <div class="form-group">
                        <select id="unidades_n" name="unidades_n" class="form-control">
                            <option>Escolha um Faculdade</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select id="curso_novo" name="curso_novo" class="form-control">
                            <option>Escolha um Curso</option>
                        </select>
                    </div>
                    <br>
                    <div class="form-group">
                        &nbsp;
                    </div>
                </form>
            </div>
        </div>


    </section>
    <div class="container-fluid  cid-rg0hEO5Sci" id="div_cont_n">

    </div>
</div>
<div class="divider">&nbsp;</div>
<br/>

<script type="text/javascript"
        src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js">
</script>
<script src="https://rawgit.com/RobinHerbots/Inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
<script src="js/aluno.js" type="text/javascript"></script>