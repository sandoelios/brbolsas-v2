<?php
    
   // print_r($ies);
?>
<div class="divider">&nbsp;
</div>
<form class="form-horizontal" style="margin: 100px 100px 100px 100px;">
    <fieldset>
        <div class="form-group">
            <div class="col-md-8">
                <p class="form-group control-label h4 h4">Cadastro de Faculdades</p><br>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="mantenedora">* Mantendora</label>  
            <div class="col-md-8">
                
                <select id="mantenedora" name="mantenedora" class="form-control required">
                    <option value="">...</option>
                    <?php foreach ($ies as $e):?>
                    <option value="<?=$e['id']?>"><?= utf8_encode($e['nome'])?></option>
                    <?php endforeach;?>
                </select>
            </div>
        </div>
        <!-- Form Name -->
        
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="textinput">Nome da Faculdade *</label>  
            <div class="col-md-8">
                <input id="nome" name="nome" type="text" placeholder="Digite seu nome" class="form-control input-md required" >

            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label h4" for="emal">Email *</label>  
            <div class="col-md-8">
                <input id="email" name="email" type="text" placeholder="Digite um email valido" class="form-control input-md required email">

            </div>
        </div>
                
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="origem_aluno">Modalidade *</label>
            <div class="col-md-8">
                <select id="modalidade" name="modalidade" class="form-control required" multiple>
                    
                    <option value="1">GRADUAÇÃO</option>
                    <option value="2">PÓS GRADUAÇÃO</option>
                    <option value="3">CURSOS TÉCNICO</option>
                    <option value="">IDIOMAS</option>
                </select>
            </div>
        </div>
      <div class="form-group">
            <label class="col-md-4 control-label h4" for="id_estado">* Estado</label>  
            <div class="col-md-8">
                
                <select id="id_estado" name="id_estado" class="form-control required">
                    <option value="">...</option>
                    <?php foreach ($estados as $e):?>
                    <option value="<?=$e['id']?>"><?= utf8_encode($e['nome'])?></option>
                    <?php endforeach;?>
                </select>
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="id_municipio">* Cidade</label>  
            <div class="col-md-8">
                
                <select id="id_municipio" name="id_municipio" class="form-control required">
                    <option value="0">...</option>
                                       
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="end">Endereço *</label>  
            <div class="col-md-8">
                <input id="email" name="end" type="end" placeholder="Digite endereço com rua complemento e número" class="form-control input-md required">

            </div>
        </div>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label h4" for="telefone">* Telefone</label>  
            <div class="col-md-8">
                <input id="telefone" name="telefone" type="text" placeholder="Salvador" class="form-control input-md required">

            </div>
        </div>

        
        <!-- Button -->
        <div class="form-group">

            <div class="col-md-4">
                <button id="salvar" name="salvar" class="btn btn-primary">Enviar</button>
            </div>
        </div>

    </fieldset>
</form>
<script src="js/ies.js" type="text/javascript"></script>