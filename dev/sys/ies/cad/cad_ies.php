<?php

require '../../../inc/bd.php';
require '../../../inc/funcoes.php';


if (isset($_POST)) {
    

    $campos = $_POST;
    for($i =0 ;count($campos['modalidade']) >$i;$i++)
    {
        ($i+1 == count($campos['modalidade'])) ? $modalidade.= $campos['modalidade'][$i]: $modalidade.= $campos['modalidade'][$i].",";
    }
    $cnpj = Limpar($campos['cnpj'], '/[^0-9$]/', '0') ;
    
    //print_r($cnpj);exit;
    $query = "INSERT INTO ies (nome,cnpj,email,modalidade,id_estado,id_municipio,telefone,responsavel,end) VALUES(";
    $query .= "'" . $campos['nome'] . "','" .$cnpj. "','" . $campos['email'] . "','" . $modalidade . "'," . $campos['id_estado'] .
            "," . $campos['id_municipio'] . ",'" . $campos['telefone'] . "','" . $campos['responsavel'] . "','" . $campos['end'] . "')";
     
    try {
        $save = $con->query($query);
    } catch (Exception $ex) {


        print_r($ex);
        exit;
    }


    // verifica se foi enviado um arquivo
    if ($_FILES['logo']['name'] and $_FILES['logo']['error'] == 0) {

        $arquivo_tmp = $_FILES['logo']['tmp_name'];
        $nome = $_FILES['logo']['name'];
        $ext = array('jpg', 'jpeg', 'gif', 'png');
        // Pega a extensão
        $e = explode('.', $_FILES['logo']['name']);
        // Converte a extensão para minúsculo
        $extensao = strtolower($e[1]);
        // Isso serve apenas para eu poder pesquisar dentro desta String
        if (in_array($extensao, $ext)) {

            // Evita nomes com acentos, espaços e caracteres não alfanuméricos
            $novoNome = $cnpj.'_logo.'.$extensao;
            //criar pasta de imagens da IES

            $dir = DIR_IMG . "/ies/" . $cnpj;
            if (is_dir($dir) == false) {
                try {
                    mkdir($dir, 0775, true);
                } catch (Exception $ex) {
                    print_r($ex);
                    exit;
                }
            }
            $destino = $dir . "/$novoNome";
            //  echo DIR_IMG . "\n";EXIT;print_r($destino);exit;
            try {
                move_uploaded_file($arquivo_tmp, $destino);
            } catch (Exception $ex) {
                var_dump($ex);
            }
            try {
                $local =  "imagens/ies/".$novoNome;
                $query = "update ies set logo_url='" .$local  . "' where cnpj='" . $campos['cnpj'] . "'";
                $save = $con->query($query);
            } catch (Exception $ex) {
                print_r($ex);
                exit;
            }
            
        } else
            echo "<script>alert('Você poderá enviar apenas arquivos *.jpg;*.jpeg;*.gif;*.png');</script>";
    
    }

    header("Location: ../../../adm/index.php?f=lista_ies");
}

