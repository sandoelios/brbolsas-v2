 
<div class="container-fluid">
  <h2>BOLSAS CADASTRADAS</h2>
  
  <table class="table" id="tabela">
    <thead>
      <tr>
        <th>FACULDADE</th>
        <th>CURSO</th>
        <th>MENSALIDADE</th>
        <th>DURAÇÃO</th>
        <th>BOLSAS</th>
        <th>BOLSAS DISPONIVEIS</th>
        <th>DESCONTO</th>
      </tr>
    </thead>
    <tbody>
       <?php 
       $query = "SELECT be.id,u.faculdade,curso,percentual,duracao,bolsa,bolsa_disponivel,mensalidade
                FROM unidades u 
                INNER JOIN bolsas_estudo be ON u.id=be.id_unidade
               ";
       $rs = $con->query($query);
       
       while ($bolsas = $rs->fetch_assoc()):
       ?>
        <tr>
        
        <td><?= $bolsas['faculdade']?></td>
        <td><?= $bolsas['curso']?></td>
        <td><?=$bolsas['mensalidade']?></td>
        <td><?=$bolsas['duracao']?></td>
        <td><?=$bolsas['bolsa']?></td>
        <td><?=$bolsas['bolsa_disponivel']?></td>
        <td><?=$bolsas['percentual']?></td>
        </tr>
        <?php endwhile;?>
        
        <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        </tr>
    </tbody>
  </table>
</div>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>

$('#tabela').dataTable( {
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
            }
        } );
</script>