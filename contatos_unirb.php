<?php
$origem = $_GET['o'];
if(!$origem) $origem = "formulario_web";
?>

<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <style>
        body{
            color: #006cd1;
           
        }
       
        #form{
              // border: 2px solid red;
              // width: 100%;
            
        }
        .form-group
        {
              padding: 5px 5px;
        }
        .control-label{
            text-transform: uppercase;
            box-shadow: black;
        }
        .btn-lg{
            width: 40%;
            left: 50%;
        }
        .img{
            width: 80%;
            left: 50%;
        }
    </style>
    <body>
        <div class="container" >
            <div class="row-fixed">
               
                    <fieldset >

                        <input type="text" value="<?=$origem?>" hidden="" id="origem" name="origem">
                    <div class="row">
                        <div class="col-md-4">
                            <h1 class="text-left">Bem-vindo</h1> 
                            <h3 class="text-left h5">Para saber mais sobre nossos cursos, formas de ingresso e bolsas de estudo, deixe aqui seu contato. </h3>
                        </div> 
                        <div class="col-md-4">
                            <img class="img" src="img/logo_unirb_2019.jpg">
                        </div><legend class="h3">  
                    </div>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="control-label" for="nome">Nome</label>  
                        <div class="col-md-8">
                            <input id="nome" name="nome" type="text" placeholder="" class="form-control input-md" required="">
                        </div>
                    </div>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="control-label" for="telefone">Telefone</label>  
                        <div class="col-md-8">
                            <input id="telefone" name="telefone" type="number" placeholder="99 99999-9999" class="form-control input-md" required="">
                        </div>
                    </div>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="control-label" for="email">E-mail</label>  
                        <div class="col-md-8">
                            <input id="email" name="email" type="email" placeholder="" class="form-control input-md" required="">
                        </div>
                    </div>
                       <div class="form-group">
                        
                        <div class="col-md-8">
                            <p id="alert" class="alert-success"></p>
                        </div>
                    </div>

                    <!-- Button -->
                    <div class="form-group">
                        <div class="col-md-8">
                            <button id="salvar" name="salvar" class="btn btn-success btn-lg">Enviar</button>
                        </div>
                    </div>
                </fieldset>
            
            </div>
        </div>
    </body>   
</html>


<script>
    $(document).ready(function(){
        
        
        $("#salvar").click(function()
        {
            origem      = $("#origem").val();    
            telefone    = $("#telefone").val();
            email       = $("#email").val();
            nome        = $("#nome").val();
            
               
            if(origem != "" && telefone != "" && nome != "" && email != "")
            {
                url =  'http://unirbonline.api.crmeducacional.com/api/IntegracaoFormularioInteresse/SalvarDados';
                $.ajax({
                    type:"POST",
                    url:url,
                    data:{
                           emailaddress1:email,
                           unirb_origem_unirb: origem,
                           fullname: nome,
                           mobilephone:telefone
                        },
                    error:function(err)
                    {
                       console.log(err.responseText);
                    },
                    dataType:'json',
                    success:function(data)
                    {
                        if(data.Status == 'Sucesso')
                        {
                            $("#alert").html("<h3>Mensagem enviada com sucesso!</h3>");
                        }
                    },
                    beforeSend: function()
                    {
                         $("#alert").html("<h3>Enviando..</h3>");
                         
                    }
                });
                
                return false;
                      
            }
            else
            {
                 $("#alert").html("<h3>Favor Preencher Formulário! .</h3>");
            }
        });
        
        
    });
</script>