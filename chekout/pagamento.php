<?php

require '../../dev/source/Library.php';

//Instantiate a new Boleto Object
$boleto = new \PagSeguro\Domains\Requests\DirectPayment\Boleto();

// Set the Payment Mode for this payment request
$boleto->setMode('DEFAULT');

