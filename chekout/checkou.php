<?php

require '../../dev/source/Library.php';

\PagSeguro\Library::initialize();
\PagSeguro\Library::cmsVersion()->setName("Nome")->setRelease("1.0.0");
\PagSeguro\Library::moduleVersion()->setName("Nome")->setRelease("1.0.0");

//Instantiate a new Boleto Object
$boleto = new \PagSeguro\Domains\Requests\DirectPayment\Boleto();

// Set the Payment Mode for this payment request
$boleto->setMode('DEFAULT');

